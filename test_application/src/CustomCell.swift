//
//  CustomCell.swift
//  test_application
//
//  Created by Admin on 1/5/17.
//  Copyright © 2017 alkurop. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
 
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var url: UILabel!
    @IBOutlet weak var snippet: UILabel!
	
}
