//
//  SearchViewDelegate.swift
//  test_application
//
//  Created by alkurop on 1/25/17.
//  Copyright © 2017 alkurop. All rights reserved.
//

import UIKit

class SearchViewDelegate : NSObject, UISearchBarDelegate {
	var query : String = ""
	let callback :(String) -> ()
	
	init(callback :@escaping (String) -> () ){
		self.callback = callback
	}
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		 _ = callback(query)
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		query = searchText
	}

}
