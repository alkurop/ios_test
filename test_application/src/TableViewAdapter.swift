//
//  TableViewAdapter.swift
//  test_application
//
//  Created by Admin on 1/5/17.
//  Copyright © 2017 alkurop. All rights reserved.
//

import UIKit

class TableViewAdapter: NSObject,  UITableViewDataSource, UITableViewDelegate{
	private weak var tableView: UITableView!
	private var content: [SearchItem] = []
	var onLinkClick : ((String) -> ())? = nil

	init(tableView: UITableView ){
		self.tableView = tableView
	}
	
	func update(list : [SearchItem]){
		list.forEach{it in
			self.content.append(it)
		}
		tableView.reloadData()
	}
	
	func clear(){
		 content.removeAll()
		 tableView.reloadData()
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
		cell.name.text = content[indexPath.row].name
		cell.snippet.text = content[indexPath.row].snippet
		cell.url.text = content[indexPath.row].url
		return cell
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return content.count
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		onLinkClick?(content[indexPath.row].url)
	}
  }
