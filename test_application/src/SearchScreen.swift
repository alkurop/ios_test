//
//  SearchScreen.swift
//  test_application
//
//  Created by Admin on 1/29/17.
//  Copyright © 2017 alkurop. All rights reserved.
//

import UIKit
import RealmSwift

class SearchScreen: Object {
	let searchItems = List<SearchItem> ()
	dynamic var query = ""
	
	static func initSearchScreen(query: String, searchItems:[SearchItem]){
		let searchScreen = SearchScreen()
		searchScreen.query = query
		searchItems.forEach { item in
			searchScreen.searchItems.append(item)
		}
	}
	dynamic var compoundKey: String = "id"
	
	override static func primaryKey() -> String? {
		return "compoundKey"
	}
}
