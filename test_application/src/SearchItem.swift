//
//  SearchItem.swift
//  test_application
//
//  Created by alkurop on 1/25/17.
//  Copyright © 2017 alkurop. All rights reserved.
//

import UIKit
import RealmSwift

class SearchItem  : Object{
	dynamic var id = ""
	dynamic var name = ""
	dynamic var url = ""
	dynamic var snippet = ""

static func initSearchItem(input: [String:Any]) -> SearchItem?{
	let searchItem = SearchItem()
	guard  let  id  = input["id"] as? String else {
		return nil
	}
	guard  let  name  = input["name"] as? String else {
		return nil
	}
	guard  let  url  = input["url"] as? String else {
		return nil
	}
	guard  let  snippet  = input["snippet"] as? String else {
		return nil
	}
	searchItem.name = name
	searchItem.id = id
	searchItem.url = url
	searchItem.snippet = snippet
	return searchItem
}
}
