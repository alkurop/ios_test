//
//  ViewController.swift
//  test_application
//
//  Created by Admin on 1/4/17.
//  Copyright © 2017 alkurop. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class ViewController: UIViewController {

    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var table: UITableView!

    var searchDelegate: SearchViewDelegate!
    var adapter: TableViewAdapter!
	var searchScreen : SearchScreen!
	let realm = try! Realm()

	func loadState(){
		if let searchScreenQuerry = realm.object(ofType: SearchScreen.self, forPrimaryKey: "id"){
			searchScreen = searchScreenQuerry
		}else{
			searchScreen = SearchScreen()
		}
		searchbar.text = searchScreen.query
		adapter.clear()
		let items = Array(searchScreen.searchItems)
		adapter.update(list: items)
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		adapter = TableViewAdapter(tableView: table)
		adapter.onLinkClick = {  link in
			if let url = URL(string: link){
				UIApplication.shared.openURL(url)
			}
		}
        table.delegate = adapter
        table.dataSource = adapter
        searchDelegate = SearchViewDelegate(callback: {
            [weak self] query in
			try!self?.realm.write{
				self?.searchScreen.searchItems.removeAll()
				self?.searchScreen.query = query
			}
            self?.adapter.clear()
            self?.executeQuery(input: query, count: 20, offset: 0)
            self?.executeQuery(input: query, count: 20, offset: 20)
        })
        searchbar.delegate = searchDelegate
		
    }
 
	override func viewDidAppear(_ animated: Bool) {
		loadState()
	}

    func executeQuery(input: String, count: Int, offset: Int) {
        let searchUrl = "https://api.cognitive.microsoft.com/bing/v5.0/search?q=\(input)&count=\(count)&offset=\(offset)&mkt=en-us"
        let headers = [
                "Ocp-Apim-Subscription-Key": "2f49d8f558a548e2b0f64843ec89d25f",
        ]

        Alamofire.request(searchUrl, headers: headers).responseJSON {
            [weak self]  response in

            var resultArray = [SearchItem]()

            guard let JSON = response.result.value as? [String:Any] else {
                return;
            }
            guard let webPages = JSON["webPages"] as? [String:Any] else {
                return;
            }
            guard  let value = webPages["value"] as? [Any] else {
                return
            }
            value.forEach({
                itemJson in
                guard let item = itemJson as? [String:Any] else {
                    return
                }
                if let searchItem = SearchItem.initSearchItem(input: item) {
                    resultArray.append(searchItem)
                }
            })
            self?.updateListWithNewItems(items: resultArray)
        }
    }

    func updateListWithNewItems(items: [SearchItem]) {
		try! realm.write {
			items.forEach{item in
				searchScreen.searchItems.append(item)
			}
		}
        adapter.update(list: items)
    }
}

